Role Name
=========

A role that installs OpenSearch, <https://www.opensearch.org>.
OpenSearch is a fork of ElasticSearch

Role Variables
--------------

The most important variables are listed below:

``` yaml
opensearch_install: true
opensearch_enabled: true
opensearch_major_version: 2
opensearch_minor_version: 19
opensearch_patch_version: 1
opensearch_version: '{{ opensearch_major_version }}.{{ opensearch_minor_version }}.{{ opensearch_patch_version }}'
opensearch_versioned: 'opensearch-{{ opensearch_version }}'
opensearch_filename: '{{ opensearch_versioned }}-linux-x64.tar.gz'
opensearch_download_url: 'https://artifacts.opensearch.org/releases/bundle/opensearch/{{ opensearch_version }}/{{ opensearch_filename }}'
opensearch_cli_tools_file: 'opensearch-cli-1.1.0-linux-x64.zip'
opensearch_cli_tools_url: 'https://artifacts.opensearch.org/opensearch-clients/opensearch-cli/{{ opensearch_cli_tools_file }}'
opensearch_user: opensearch
opensearch_base_install_dir: /opt/opensearch
opensearch_config_dir: '{{ opensearch_base_install_dir }}/config'
opensearch_bin_dir: '{{ opensearch_base_install_dir }}/bin'
opensearch_cluster_name: 'Opensearch Cluster'
opensearch_http_port: 9200
opensearch_transport_min_port: 9300
opensearch_transport_max_port: 9400
opensearch_data_directory: /var/lib/opensearch
opensearch_log_dir: /var/log/opensearch
opensearch_bind_ip: 0.0.0.0
opensearch_single_node: true
opensearch_discovery_host_list: '["127.0.0.1", "[::1]"]'
opensearch_define_majority_of_nodes: true
opensearch_majority_of_nodes: 1
opensearch_bootstrap_known_masters:
  - '{{ ansible_fqdn }}'
opensearch_real_cluster: false
opensearch_recover_after_nodes: 3
opensearch_max_local_storage_nodes: 1
opensearch_destructive_requires_name: true
opensearch_define_heap_size: false
opensearch_heap_size: 2g
opensearch_additional_java_opts: '-server -Djava.awt.headless=true -Dfile.encoding=UTF-8'
opensearch_java_io_tmpdir: '/var/tmp'
opensearch_additional_conf:
  - {name: 'search.max_buckets', value: '65535'}
opensearch_max_open_files: 65535
opensearch_max_processes: 8192
opensearch_cluster_routing_allocation_disk_threshold_enabled: 'true'
opensearch_cluster_routing_allocation_disk_watermark_low: '85%'
opensearch_cluster_routing_allocation_disk_watermark_high: '90%'
# Compatibility with kernels <= 3.5. Set to False if you are using a newer kernel
opensearch_disable_bootstrap_syscall_filter: true
opensearch_security_enabled: false

```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
